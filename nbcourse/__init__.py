"""
Build a small website to host Jupyter notebooks as course chapters
"""
import logging

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

__version__ = '0.3.6'
