"""
python -m nbcourse module entry point to run via python -m
"""

from .main import main


if __name__ == '__main__':
    main()
